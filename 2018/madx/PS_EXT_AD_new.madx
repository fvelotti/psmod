/******************************************************************
 * MAD-X PS Optics
 **
 ** AD extraction
 **
 ** Alexander Huschauer
 ** Created 08/02/18

 ** F.Velotti: adding KFAs and caluclating initial condition for TT2 
 ** with extraction elements on
 ******************************************************************/
option, RBARC=FALSE;

/******************************************************************
 * Call lattice files
 ******************************************************************/
call, file="../beams/ps_beam_26GeV.beamx";
call, file="../elements/PS.ele";
call, file="../sequence/PS.seq";
call, file="../strength/elements.str";
call, file="../strength/PS_EXT_AD.str";
call, file="../cmd/macros.ptc";

call, file="../cmd/fringe_field_fix.madx";

/******************************************************************
 * Twiss with bump on
 ******************************************************************/

use, sequence=PS;
twiss, file = '../output/twiss/PS_EXT_AD.twiss';

/**********************************************
* Make extraction sequence to get to where there
* is the handover with TT2
**********************************************/

! Nominal kick strength for KFA71 for nTOF => assumed the same for AD
kfa71_nom_kick = -1.85e-03;

! Maybe this is redundant but PS sequence definition not very clear
KFA7179: HKICKER, L:=PE.KFA71->L, KICK:= kfa71_k;
KFA45 :  HKICKER, L:=PI.KFA45->L, KICK:= kfa45_k;
KFA28 :  HKICKER, L:=PI.KFA28->L, KICK:= kfa28_k;

SEQEDIT, sequence=PS;
FLATTEN;
REPLACE, ELEMENT=PI.KFA45, BY=KFA45;
REPLACE, ELEMENT=PI.KFA28, BY=KFA28;
REPLACE, ELEMENT=PE.KFA71, BY=KFA7179;
FLATTEN;
ENDEDIT;

use, sequence = PS;

! Cycle sequence and changing starting point

SEQEDIT, sequence=PS;
CYCLE, START=PR.BPM17;
FLATTEN;
ENDEDIT;

use, sequence=PS;
twiss;

start_psej = table(twiss, PR.BPM23, s);
end_psej = table(twiss, PE.BEGSMH16, s);

len_psej = end_psej - start_psej;

value, len_psej;

/* Hack from Pablo to stitch F16: we are extracting */

len_ft16 = 7.701173928;

/**********************************************************
** Declaration of stray filed found in sequence/PS.seq

SEPTUM16: RBEND,L=2.4,ANGLE=-0.027;
D16STRAY: RBEND,L=2.20480,ANGLE=0.015,K1=-.047,K2=0.33;
F16SHIM : RBEND,L=2.19624,ANGLE=0.021,K1=0.0;

**********************************************************/

SEPTUM16E: MARKER;
FT16: sequence, refer=entry, l=len_ft16;
MTV001        ,at=0.0;
SEPTUM16      ,at=0.0;
SEPTUM16E       ,at=2.700072902;
D16STRAY      ,at=2.700072902;
F16SHIM       ,at=4.904893572;
pointR        ,at=7.701173928;
ENDSEQUENCE;

! POINTR is the handover point between PS and TT2 => initial conditions

/***********************************
* PS_EJ sequence definition
***********************************/

EXTRACT, sequence=PS, FROM=PR.BPM23, TO=PE.BEGSMH16, newname=PS_EJ;


PS_EXTRACT: sequence, refer=entry, l=len_psej + len_ft16 ;
PS_EJ,       at=0.0;
FT16,        at=len_psej;
ENDSEQUENCE;

kfa71_k = 0.0;
kfa45_k = 0;
kfa28_k = 0;

USE, sequence=PS;
SELECT, FLAG=TWISS, COLUMN=NAME,KEYWORD,S,BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,KMIN,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;
savebeta, label=bumped, place = PR.BPM23;
twiss, file = "../output/TT2/PS_EXT_AD_bump.twiss";

use, sequence = PS_EXTRACT;

twiss, beta0 = bumped, file = "../output/TT2/PS_to_AD_bumped.twiss";

kfa71_k = kfa71_nom_kick;
kfa45_k = 0;
kfa28_k = 0;


twiss, beta0 = bumped;
x_stray_field = table(twiss, SEPTUM16E, X);
px_stray_field = table(twiss, SEPTUM16E, PX);

exec, place_stray_field(x_stray_field, px_stray_field);

savebeta,label=initial_cond, place = POINTR;
twiss, beta0 = bumped, file = "../output/TT2/PS_to_AD_kicked.twiss";

/***********************************************
* Save initial parameters to file for TL usage
***********************************************/
assign, echo="../output/TT2/tt2_AD_from_stitched_kickers.inp";

betx0 = initial_cond->betx;
bety0 =  initial_cond->bety;

alfx0 = initial_cond->alfx;
alfy0 = initial_cond->alfy;

dx0 = initial_cond->dx;
dy0 = initial_cond->dy;

dpx0 = initial_cond->dpx;
dpy0 = initial_cond->dpy;

print, text="/*********************************************************************";
print, text='Initial conditions from MADX stitched model of PS extraction to nTOF';
print, text="*********************************************************************/";

print, text = '';
value,betx0;
value,bety0;
      
value,alfx0;
value,alfy0;
      
value,dx0 ;
value,dy0 ;
      
value,dpx0;
value,dpy0;

assign, echo=terminal;

stop;










